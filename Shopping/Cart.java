package Shopping;

import java.util.ArrayList;
import java.util.Scanner;

public class Cart implements shop {
    ArrayList<Iterm> iterm = new ArrayList<Iterm>();
	private Scanner scan = new Scanner(System.in);
	public  void cartmenu(Iterm i) {
		println("1.删除商品");
		println("2.添加商品");
		println("3.结算全部商品");
		println("4.清空购物车");
		println("5.显示购物车中所有商品");
		println("0.返回菜单");
		println("请选择：");
		int c = nextInt();
		switch (c) {
		case 1: {
			deleteproduct(i);
			break;
		}
		case 2: {
			addproduct(i);
			break;
		}
		case 3: {
			System.out.println(allmoney());
			break;
		}
		case 4: {
			iterm.clear();
			break;
		}
		case 5:{
			for(Iterm i1:iterm){
	    		System.out.println(i1.toString());
	    		break;
	    	}
		}
		case 0: {
			return;
		}
		}
	}

	public void addproduct(Iterm i) {
		if(iterm.contains(i)){
			iterm.get(iterm.indexOf(i)).number+=i.number;
		}
		else iterm.add(i);
	}

	public void deleteproduct(Iterm i) {
		iterm.remove(i);
	}

	public double allmoney() {
		double sum = 0;
		for (Iterm i : iterm) {
			sum += i.getPrice() * i.getNumber();
		}
		return sum;
	}

	public  void showcartproduct() {
		for (Iterm i : iterm) {
			println(i.toString());
		}
	}

	public void print(String text) {

		System.out.print(text);
		

	}

	public void println(String text) {
		System.out.println(text);

	}

	public int nextInt() {
		return scan.nextInt();
	}

	public Double nextDouble() {
		return scan.nextDouble();
	}

	public String next() {
		return scan.next();
	}
}
