package Shopping;

public interface shop {
	public void print(String text);
    public abstract void println(String text);
    public abstract int nextInt();
    public abstract Double nextDouble();
    public abstract String next();
}
