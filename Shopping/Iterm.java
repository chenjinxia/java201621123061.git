package Shopping;

public class Iterm{
	private String name;
	private double price;
	public int number;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public Iterm(String name, double price, int number) {
		super();
		this.name = name;
		this.price = price;
		this.number = number;
	}
	@Override
	public String toString() {
		return "商品名：" + name + " 价格：" + price + " 数量" + number ;
	}
	public String toString2() {
		return "商品名：" + name + " 价格：" + price;
	}
	public static Iterm[] allproduct(){
		Iterm[] list=new Iterm[7];
		list[0]=new Iterm("书包",129.9,0);
		list[1]=new Iterm("裙子",99.9,0);
		list[2]=new Iterm("Java学习笔记",68.0,0);
		list[3]=new Iterm("小米笔记本pro",6198.0,0);
		list[4]=new Iterm("小白鞋",27.6,0);
		list[5]=new Iterm("保温杯",30.0,0);
		list[6]=new Iterm("活着",10.0,0);
		return list;
		
	}
	
	
}
