package week9;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.lang.String;


public class Main1 {

	public static List<String> convertStringToList(String line) {
		List<String> a = new ArrayList<String>();
        String[] arr = line.split(" +");
		for (int i = 0; i < arr.length; i++) {
			a.add(arr[i]);
		}
		return a;
	}

	public static void remove(List<String> list, String str) {
		for (int i = list.size()-1; i >=0; i--) {
			if(list.get(i).equals(str))
				list.remove(i);
		}
	}
	
     public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextLine()){
            List<String> list = convertStringToList(sc.nextLine());
            System.out.println(list);
            String word = sc.nextLine();
            remove(list,word);
            System.out.println(list);
        }
        sc.close();
    }
}
