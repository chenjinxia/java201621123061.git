package week9;

import java.util.Map;
import java.util.Map.Entry;
import java.util.*;

public class Main4 {
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		Map<String,Integer> map=new HashMap<String,Integer>();
		while(true){
			String str=sc.next();
			if(str.equals("!!!!!")){
				break;
			}
			if(map.containsKey(str))
				map.put(str, map.get(str)+1);
			if(map.get(str)==0)
				map.put(str, 1);
		}
		System.out.println(map.size());
		List<Entry<String,Integer>> list = new ArrayList<Entry<String,Integer>>(map.entrySet());
		Collections.sort(list,new Comparator<Map.Entry<String,Integer>>(){
			public int compare(Map.Entry<String, Integer> o1,Map.Entry<String,Integer> o2){
				if(o1.getValue()!=o2.getValue()){
					return o2.getValue()-o1.getValue();
				}
				else{
					return o1.getKey().compareTo(o2.getKey());
				}
			}
		});
	sc.close();
	for(int i=0;i<10;i++){
		System.out.println(list.get(i));
	}

	}
}
