package week9;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Scanner;

public class Main5 {

	public static void main(String[] args) {
		
		Scanner in=new Scanner(System.in);
		Deque<Integer> queueA=new LinkedList<>();
		Deque<Integer> queueB=new LinkedList<>();
		int n=in.nextInt();
		for(int i=0;i<n;i++) {
			int m=in.nextInt();
			if(m%2==0)
				queueB.addLast(m);
			else queueA.addLast(m);
		}
		if(queueA.size()<2) {
			System.out.println(queueA.removeFirst());
			while(!queueB.isEmpty())
				System.out.print(" "+queueB.removeFirst());
		}
		else {
		System.out.print(queueA.removeFirst());
		System.out.print(" "+queueA.removeFirst());
		System.out.print(" "+queueB.removeFirst());
		}
		if(queueB.isEmpty()) {
			System.out.print(queueA.removeFirst());
			while(!queueA.isEmpty())
				System.out.print(" "+queueA.removeFirst());
		}
		while(!queueA.isEmpty()&& !queueB.isEmpty()) {
			System.out.print(" "+queueA.removeFirst());
			System.out.print(" "+queueA.removeFirst());
			System.out.print(" "+queueB.removeFirst());
		}
		while(!queueA.isEmpty()) {
			System.out.print(" "+queueA.removeFirst());
		}
		while(!queueB.isEmpty()) {
			System.out.print(" "+queueB.removeFirst());
		}
		
	}
}