package week9;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

interface IntegerStack{
	public Integer push(Integer item); //如item为null，则不入栈直接返回null。否则直接入栈，然后返回item。
	public Integer pop();              //出栈，如栈为空，则返回null。
	public Integer peek();             //获得栈顶元素，如栈顶为空，则返回null。注意：不要出栈
	public boolean empty();           //如过栈为空返回true
	public int size();                //返回栈中元素数量
}

class ArrayListIntegerStack implements IntegerStack{
	private List<Integer> list;
	
	public ArrayListIntegerStack(){//在无参构造函数中新建ArrayList或者LinkedList，作为栈的内部存储。
		list=new ArrayList<Integer>();
	}
	
	public Integer push(Integer item){
		if(item==null)
			return null;
		else
			list.add(item);
		return item;
	}

	@Override
	public Integer pop() {
		if(list.isEmpty())
			return null;
		else
			return list.remove(list.size()-1);
	}

	@Override
	public Integer peek() {
		if(list.isEmpty())
			return null;
		else
		return list.get(list.size()-1);
	}

	@Override
	public boolean empty() {
		if(list.isEmpty())
			return true;
		else
			return false;
	}

	@Override
	public int size() {
		return list.size();
	}
	
	public String toString(){
		return list.toString();
	}
}

public class Main2{
	public static void main(String[] args){
		ArrayListIntegerStack list=new ArrayListIntegerStack();
		Scanner sc = new Scanner(System.in);
		int m= sc.nextInt();
		for(int i=0;i<m;i++){
			System.out.println(list.push(sc.nextInt()));
		}
		System.out.println(list.peek()+","+list.empty()+","+list.size());
		System.out.println(list.toString());
		int x=sc.nextInt();
		for(int i=0;i<x;i++){
			System.out.println(list.pop());
		}
		System.out.println(list.peek()+","+list.empty()+","+list.size());
		System.out.println(list.toString());
		sc.close();
	}
}
