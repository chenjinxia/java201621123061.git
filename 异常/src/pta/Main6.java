package pta;

import java.util.Scanner;

//7-3 jmu-Java-06异常-03-throw与throws（10 分）
class ArrayUtils{
	public static double findMax(double[] arr,int begin, int end) throws IllegalArgumentException{
		if(begin>=end)
			throw new IllegalArgumentException("begin:"+begin+">="+"end:"+end);
		else if(begin<0)
			throw new IllegalArgumentException("begin:"+begin+"<0");
		else if(end>arr.length)
			throw new IllegalArgumentException("end:"+end+">"+"arr.length");
		double max=arr[begin+1];//findMax方法用来返回arr数组中在下标begin（不包含begin）与end-1之间（包括end-1）的最大值。
		for(int i=begin+1;i<end;i++){
			if(max<arr[i]){
				max=arr[i];
			}
		}
		return max;
	}
}

//抛出异常在方法中是throw，在主函数main中时try-catch
public class Main6{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();//输入n，创建大小为n的double数组。
		double[] arr=new double[n];
		for(int i=0;i<n;i++){
			arr[i]=sc.nextDouble();		
		}//输入n个浮点型整数，放入数组。
		while(sc.hasNextInt()){
			int begin=sc.nextInt();
			int end=sc.nextInt();
			try{
				System.out.println(ArrayUtils.findMax(arr, begin, end));
			}catch(Exception e){
				System.out.println(e);
			}
		}
		try {
		     System.out.println(ArrayUtils.class.getDeclaredMethod("findMax", double[].class,int.class,int.class));
		} catch (Exception e1) {
		}
	}
}