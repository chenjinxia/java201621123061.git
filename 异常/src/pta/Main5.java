package pta;

import java.util.Arrays;
import java.util.Scanner;

//7-2 jmu-Java-06异常-02-使用异常机制处理异常输入（5 分）
public class Main5 {
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		int[] arr=new int[n];
		for(int i=0;i<n;){
			try{//非整形字符不能被转为整形，所以会抛出异常
				String input=sc.next();//输入字符，包括整数或者非整形字符
				arr[i]=Integer.parseInt(input);//将所有字符转为整形。
				i++;
			}catch(Exception e){
				System.out.println(e);
			}
		}
		System.out.println(Arrays.toString(arr));//使用Arrays.toString输出数组中的内容。
	}
}
