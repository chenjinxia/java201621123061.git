package pta;

import java.util.Scanner;

//6-2 jmu-Java-06异常-finally（8 分）

class Resource{
	public void open(String str) throws Exception{
		return ;
	}
	
	public void close() throws RuntimeException{
		return ;
	}
}
public class Main2 {
	public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
	    Resource resource = null;
	    try{
	        resource = new Resource();
	        resource.open(sc.nextLine());
	         /*这里放置你的答案*/
	        System.out.println("resource open success");
	    }catch(Exception e){
	    	System.out.println(e);//输出异常
	    }finally{
	    	try{
	    		resource.close();
	    		System.out.println("resource release success");
	    	}catch(RuntimeException e){
	    		System.out.println(e);//输出异常
	    	}
	    }
	       sc.close();

	}
}
