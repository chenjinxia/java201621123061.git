package pta;

import java.util.NoSuchElementException;
import java.util.Scanner;


class IllegalScoreException extends Exception{
	public IllegalScoreException(String name){
		super (name);
	}
}

class IllegalNameException extends RuntimeException{
	public IllegalNameException(String score){
		super(score);
	}
}

class Student{

	private String name;
	private int score;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(name.toCharArray()[0]>'0'&&name.toCharArray()[0]<'9'){		
			throw new IllegalNameException("the first char of name must not be digit, name="+name);
		}
		this.name = name;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
	public int addScore(int score) throws IllegalScoreException{
		if(score < 0 || score > 100){
			throw new IllegalScoreException("score out of range, score="+score);
			
		}
		return score;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", score=" + score + "]";
	}

}

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
	while(in.hasNext()){
		if(in.next().equals("new")){
			try{
				Student stu = new Student();
				stu.setName(in.next());
				int score = in.nextInt();
				stu.setScore(score);
				stu.addScore(score);
				System.out.println(stu.toString());
			}catch(NoSuchElementException e){
				System.out.println("java.util.NoSuchElementException");
			}catch(IllegalNameException e){
				System.out.println(e);
				break;
			} catch(IllegalScoreException e){
				System.out.println(e);
				}
			}
			
		}
	in.close();
	System.out.println("scanner closed");
	}
}
