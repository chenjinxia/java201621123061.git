package pta;
//6-3 jmu-Java-06异常-ArrayIntegerStack异常改进版（5 分）
//改造!!接口!!章节的ArrayIntegerStack，为其pop()、push()、peek()方法添加出错时抛出异常的功能。
//栈有n个元素。top指针范围为1~n，数组arrStack底数0~n-1，栈顶元素为arrStack[top-1].
import java.util.Arrays;
import java.util.Scanner;

interface IntegerStack {
	public Integer push(Integer item) ;
	public Integer pop(); 
	public Integer peek(); 
	public boolean empty(); 
	public int size(); 
}
 
 class ArrayIntegerStack implements IntegerStack{
		    private int capacity;//代表内部数组的大小
		    private int top=0;//代表栈顶指针。栈空时，初始值为0。
		    private Integer[] arrStack;//用于存放元素的数组
		    /*其他代码*/
		    /*你的答案，即3个方法的代码*/
			
		

	@Override
	public Integer push(Integer item) throws FullStackException{
		if(item==null)
			return null;
		if(top==arrStack.length)//重点理解，联系当top=0时栈为空，数组长度也为空。
			throw new FullStackException();
		arrStack[top++]=item;//理解，当item进数组时，top立马变为1了，所以这里要top++；
		return item;
		}

	

	@Override
	public Integer pop() throws EmptyStackException{
		if(top==0)//栈空
			throw new EmptyStackException();
		top--;//出栈
		return arrStack[top];
	}
		

	@Override
	public Integer peek() throws EmptyStackException{
		if(top==0)//栈空
			throw new EmptyStackException();
		return arrStack[top-1];//返回栈顶元素。
		
	}

	@Override
	public boolean empty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}
 }
 
 
 
public class Main3{
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		ArrayIntegerStack stack = new ArrayIntegerStack(n);
		int m = in.nextInt();
		for (int i = 0; i < m; i++) {
			System.out.println(stack.push(in.nextInt()));
		}
		System.out.println(stack.peek() + "," + stack.empty() + ","+ stack.size());
		System.out.println(Arrays.toString(stack.getArr()));
		int x = in.nextInt();
		for (int i = 0; i < x; i++) {
			System.out.println(stack.pop());
		}
		System.out.println(stack.peek() + "," + stack.empty() + ","+ stack.size() );
		System.out.println(Arrays.toString(stack.getArr()));
	}

}