package pta;

import java.util.Scanner;

//7-1 jmu-Java-06异常-01-常见异常（5 分）
public class Main4 {
	public static void main(String[] args){
		int[] arr=new int[5];
		String str=null;
		Scanner sc=new Scanner(System.in);
		while(sc.hasNext()){
			String a=sc.next();
			try{
				if(a.equals("arr")){
					int int1=sc.nextInt();
					arr[int1]=1;	//输入下标	
			}
			else if(a.equals("null")){
				str.length();//str为空，所以str.length()是错误的，就会抛出异常。
			}
			else if(a.equals("cast")){//字符不能被强制转为数字，所以会产生异常
				Object o=new String("str");
				Integer x=(Integer)o;//强制转换
			}
			else if(a.equals("num")){//字符不能转为数字，所以会产生异常
				String str1=sc.next();
				Integer integer1=Integer.parseInt(str1);
			}else break;//其他条件下跳出循环
			}catch(Exception e){
				System.out.println(e);
			}
	}
}
}