package pta;

import java.util.Scanner;
import java.util.NoSuchElementException;
//7-4 jmu-Java-06异常-04-自定义异常(综合)（15 分）
//String.toCharArray()返回一个新分配的字符数组，它的长度是原来字符串的长度，它的内容被初始化为包含此字符串表示的字符序列。
class IllegalScoreException extends Exception{
	public IllegalScoreException(String score){
		super(score);
	}
}

class IllegalNameException extends RuntimeException{
	public IllegalNameException(String name){
		super(name);
	}
}

class Student{
	private String name;
	private int score;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		if(name.toCharArray()[0]>'0'&& name.toCharArray()[0]<'9')
			throw new IllegalNameException("the first char of name must not be digit, name="+name);
		this.name = name;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", score=" + score + "]";
	}
	
	public int addScore(int score) throws IllegalScoreException{
		if(score<0||score>100)
			throw new IllegalNameException("score out of range, score="+score);
		return score;						
	}	
}

public class Main7{
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		while(sc.hasNext()){
			if(sc.next().equals("new")){
				try{
					Student stu=new Student();//新建学生
					String name=sc.next();
					stu.setName(name);//输入一个值并调用setname。
					int score=sc.nextInt();
					stu.setScore(score);//输入一个值并调用setname。
					stu.addScore(score);//
					System.out.println(stu.toString());
				}catch(NoSuchElementException e){
					System.out.println("java.util.NoSuchElementException");
				}catch(IllegalNameException e){
					System.out.println(e);
					break;}
				catch(IllegalScoreException e){
					System.out.println(e);
					}

		}
	}
		sc.close();
		System.out.println("scanner closed");
}
}

