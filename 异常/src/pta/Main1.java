package pta;

import java.util.Scanner;

//6-1 jmu-Java-06异常-多种类型异常的捕获（3 分）
//如果try块中的代码有可能抛出多种异常，且这些异常之间可能存在继承关系，那么在捕获异常的时候需要注意捕获顺序。
//子类异常需放在父类异常之前
public class Main1 {
	public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
	    while (sc.hasNext()) {
	        String choice = sc.next();
	        try {
	            if (choice.equals("number"))
	                throw new NumberFormatException();
	            else if (choice.equals("illegal")) {
	                throw new IllegalArgumentException();
	            } else if (choice.equals("except")) {
	                throw new Exception();
	            } else
	            break;
	        }catch(NumberFormatException e){
	        	System.out.println("number format exception");
	        	System.out.println(e);//输出java.lang.NumberFormatException
	        }
	        catch(IllegalArgumentException e){
	        	System.out.println("illegal argument exception");
	        	System.out.println(e);//输出java.lang.IllegalArgumentException
	        }
	        catch(Exception e){
	        	System.out.println("other exception");
	        	System.out.println(e);//输出java.lang.Exception
	        }
	        
	}
}
}
