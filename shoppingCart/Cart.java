package shoppingCart;


import java.awt.Menu;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Shopping.Iterm;

public class Cart extends JFrame {

	ArrayList<Iterm> iterm = new ArrayList<Iterm>();
	private static final long serialVersionUID = 1L;
	Iterm[] list;
	public static  Iterm[] all(){
		Iterm[] list=new Iterm[7];
		list[0]=new Iterm("书包",129.9,0);
		list[1]=new Iterm("裙子",99.9,0);
		list[2]=new Iterm("Java学习笔记",68.0,0);
		list[3]=new Iterm("小米笔记本pro",6198.0,0);
		list[4]=new Iterm("小白鞋",27.6,0);
		list[5]=new Iterm("保温杯",30.0,0);
		list[6]=new Iterm("活着",10.0,0);
		return list;
	}
    public Cart(String title,ArrayList<Iterm> iterm) {
        initComponents();
        super.setTitle(title);
        this.iterm=iterm;
		this.list=Cart.all();
        //this.booklist=(new Goods("全部商品",iterm)).zhao();
    }
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {
        cartfind = new javax.swing.JButton();
        calculate = new javax.swing.JButton();
        deletegood = new javax.swing.JButton();
        cartaddgood = new javax.swing.JButton();
        clearcart = new javax.swing.JButton();
        cartback = new javax.swing.JButton();
        cartback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	Cart.this.dispose();
            }
        });
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        cartfind.setText("查看购物车商品");
        cartfind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Goods g=new Goods("全部商品",iterm);//购物车的商品
                
            }
        });
        calculate.setText("结算");
        calculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	double sum=0;
            	for (Iterm i : iterm) {
        			sum += i.getPrice() * i.getNumber();
        		}
                JOptionPane.showMessageDialog(null, sum);
            }
        });

        deletegood.setText("删除商品");
        deletegood.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	Deletegoods d=new Deletegoods("添加商品",iterm);
                d.setVisible(true);
            }
        });

        cartaddgood.setText("添加商品");
        cartaddgood.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	Addgoods adds=new Addgoods("添加商品",iterm, list);
                adds.setVisible(true);
            }
        });

        clearcart.setText("清空购物车");
        clearcart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearcartActionPerformed(evt);
            }
        });

        cartback.setText("返回");
        cartback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cart.this.dispose();
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(53, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cartaddgood, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cartfind, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                    .addComponent(calculate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(deletegood, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(clearcart, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                    .addComponent(cartback, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(76, 76, 76))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(clearcart, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(cartfind, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cartaddgood, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deletegood, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(calculate, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cartback, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(109, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>                        

    private void calculateActionPerformed(java.awt.event.ActionEvent evt) { 
    	
    }                                         

    private void deletegoodActionPerformed(java.awt.event.ActionEvent evt) {                                           
    	
    }                                          

    private void clearcartActionPerformed(java.awt.event.ActionEvent evt) {                                          
        iterm.clear();
        JOptionPane.showMessageDialog(null,"已清空");
    }                                         
   
    private void cartaddgoodActionPerformed(java.awt.event.ActionEvent evt) {                                            
        
    }                                           

    private void cartbackActionPerformed(java.awt.event.ActionEvent evt) {                                         
    	
    }                                        
                    
    private javax.swing.JButton calculate;
    private javax.swing.JButton cartaddgood;
    private javax.swing.JButton cartback;
    private javax.swing.JButton cartfind;
    private javax.swing.JButton clearcart;
    private javax.swing.JButton deletegood;
    // End of variables declaration                   
}

