package shoppingCart;

import java.util.ArrayList;
import javax.swing.JOptionPane;
import Shopping.Iterm;

public class Menu extends javax.swing.JFrame {

	public Iterm[] booklist;
	ArrayList<Iterm> iterm = new ArrayList<Iterm>();
    public Menu(String title) {
        initComponents();
        super.setTitle(title);
        Iterm[] list=new Iterm[7];
		list[0]=new Iterm("书包",129.9,0);
		list[1]=new Iterm("裙子",99.9,0);
		list[2]=new Iterm("Java学习笔记",68.0,0);
		list[3]=new Iterm("小米笔记本pro",6198.0,0);
		list[4]=new Iterm("小白鞋",27.6,0);
		list[5]=new Iterm("保温杯",30.0,0);
		list[6]=new Iterm("活着",10.0,0);
		
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        findgood = new javax.swing.JLabel();
        findtext = new javax.swing.JTextField();
        scangoods = new javax.swing.JButton();
        mycart = new javax.swing.JButton();
        menueok = new javax.swing.JButton();
        menueback = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        findgood.setText(" 查找商品");

        findtext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	
            }
        });

        scangoods.setText("浏览商品");
        scangoods.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	 Goods g=new Goods("全部商品");
            	 //Menue.this.dispose();
            }
        });

        mycart.setText("我的购物车");
        mycart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Cart car=new Cart("我的购物车",iterm);
                car.setVisible(true);
                
            }
        });
        
        menueok.setText("确认");
        menueok.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	boolean flag=false;
            	for(Iterm i:booklist){
            		if(findtext.getText().equals(i.getName())){
            			JOptionPane.showMessageDialog(null,"书名："+i.getName()+" 价格："+i.getPrice());
            			flag=true;
            			break;
            	    }
               }
            	if(!flag){
            		JOptionPane.showMessageDialog(null,"未找到！请重新输入！");
            	}
            }
        });

        menueback.setText("返回");
        menueback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Menu.this.dispose();
            }
        });
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(findgood, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(menueok, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(menueback, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(findtext, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(148, 148, 148)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(mycart, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(scangoods, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(75, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(scangoods, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(mycart, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(findgood, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(findtext, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(menueok, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(menueback, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(84, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>                        

    private void findtextActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
    }                                        

//    private void menueokActionPerformed(java.awt.event.ActionEvent evt) {                                        
//        
//    }                                       
    
    
    private void scangoodsActionPerformed(java.awt.event.ActionEvent evt) {                                          
        // TODO add your handling code here:
    }                                         

    private void menuebackActionPerformed(java.awt.event.ActionEvent evt) {                                          
        // TODO add your handling code here:
    }                                         
    @SuppressWarnings("unused")
    
	private void mycartActionPerformed(java.awt.event.ActionEvent evt) {                                       
    }   
    private javax.swing.JLabel findgood;
    private javax.swing.JTextField findtext;
    private javax.swing.JButton menueback;
    private javax.swing.JButton menueok;
    private javax.swing.JButton mycart;
    private javax.swing.JButton scangoods;
                 
}


