package shoppingCart;


import java.util.ArrayList;

import javax.swing.JOptionPane;

import Shopping.Iterm;


/**
 *
 * @author ASUS
 */
public class Goods extends javax.swing.JFrame {

	Iterm[] booklist;
	ArrayList<Iterm> iterm = new ArrayList<Iterm>();

   
	private static final long serialVersionUID = 1L;

	public  Iterm[] allproduct(){
		
		Iterm[] list=new Iterm[7];
		list[0]=new Iterm("书包",129.9,0);
		list[1]=new Iterm("裙子",99.9,0);
		list[2]=new Iterm("Java学习笔记",68.0,0);
		list[3]=new Iterm("小米笔记本pro",6198.0,0);
		list[4]=new Iterm("小白鞋",27.6,0);
		list[5]=new Iterm("保温杯",30.0,0);
		list[6]=new Iterm("活着",10.0,0);
		return list;
	}
	public Iterm[] zhao(){
		return this.booklist;
	}
	//Goods g=new Goods("全部商品",iterm);
	public Goods(String title) {
    	super.setTitle(title);
        initComponents();
        String s="";
        for(Iterm i:this.allproduct())
        	s+=i.getName()+" "+i.getPrice()+"\n";
        imformationtext.setText(s);
        this.setVisible(true);
    }
    public Goods(String title,ArrayList<Iterm> iterm) {
    	super.setTitle(title);
    	initComponents();
    	String s="";
    	for(Iterm i:iterm)
        	s+="商品名："+i.getName()+" 价格："+i.getPrice()+" 数量："+i.getNumber()+"\n";
        imformationtext.setText(s);
        this.setVisible(true);
        //initComponents();
    }
    public void findcart(ArrayList<Iterm> iterm){
    	for(Iterm i:iterm)
        	imformationtext.setText(i.toString());
    }
    public void findall(){
    	String s="";
    	for(Iterm i:this.allproduct()){
    		s=(s+i.toString2()+"\n");
    	}
    	System.out.println("s");
        imformationtext.setText(s);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        imformation = new javax.swing.JLabel();
        goodback = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        imformationtext = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        imformation.setText("    购物车中商品信息");

        goodback.setText("返回");
        goodback.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
            	Goods.this.dispose();
            }
        });
        imformationtext.setColumns(20);
        imformationtext.setRows(5);
        jScrollPane1.setViewportView(imformationtext);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(173, 173, 173)
                        .addComponent(imformation, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(284, 284, 284)
                        .addComponent(goodback, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(151, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(177, 177, 177))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(imformation, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(goodback, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(92, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>                        

    /**
     * @param args the command line arguments
     */
   

    // Variables declaration - do not modify                     
    private javax.swing.JButton goodback;
    private javax.swing.JLabel imformation;
    private javax.swing.JTextArea imformationtext;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration                   
}

