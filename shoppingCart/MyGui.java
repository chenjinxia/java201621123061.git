package shoppingCart;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class MyGui {
	//登录界面
	public static class LoginFrame extends JFrame {

		private static final long serialVersionUID = 1L;
		private JButton login;
		
		private JPasswordField passInput;
		private JLabel password;
		private JLabel user;
		private JTextField userInput;

		public LoginFrame(String title) {// 登录界面
			super(title);//调用父类
			initComponents();//在界面添加各个组件，并为它们注册监听器
			super.setTitle(title);
		}
		
		private void initComponents() {

			user = new JLabel();
			userInput = new JTextField();
			password = new JLabel();
			
			login = new JButton();
			passInput = new JPasswordField();

			setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

			user.setText("账户名：");
			password.setText("登录密码：");
			
			login.setText("登录");
			login.addActionListener(new java.awt.event.ActionListener() {
				@SuppressWarnings("deprecation")
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					if (userInput.getText().equals("chenjinxia") && passInput.getText().equals("201621123061")) {
						Menu menue=new Menu("菜单");
						menue.setVisible(true);
						LoginFrame.this.dispose();
					} else {
						JOptionPane.showMessageDialog(null, "您输入的用户名密码不正确");
					}
				}
			});

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
			getContentPane().setLayout(layout);
			layout.setHorizontalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(layout.createSequentialGroup().addGap(63, 63, 63)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
									.addComponent(password, javax.swing.GroupLayout.DEFAULT_SIZE,
											javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(user, javax.swing.GroupLayout.DEFAULT_SIZE, 63, Short.MAX_VALUE))
							.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
									.addComponent(userInput)
									.addGroup(layout.createSequentialGroup()
											
											.addGap(18, 18, 18).addComponent(login,
													javax.swing.GroupLayout.DEFAULT_SIZE, 64, Short.MAX_VALUE))
									.addComponent(passInput))
							.addContainerGap(122, Short.MAX_VALUE)));
			layout.setVerticalGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(layout.createSequentialGroup().addGap(64, 64, 64)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
									.addComponent(userInput, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
									.addComponent(user, javax.swing.GroupLayout.DEFAULT_SIZE,
											javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(18, 18, 18)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
									.addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, 26,
											javax.swing.GroupLayout.PREFERRED_SIZE)
									.addComponent(passInput, javax.swing.GroupLayout.PREFERRED_SIZE, 33,
											javax.swing.GroupLayout.PREFERRED_SIZE))
							.addGap(50, 50, 50)
							.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
									
									.addComponent(login, javax.swing.GroupLayout.DEFAULT_SIZE,
											javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addContainerGap(77, Short.MAX_VALUE)));

			pack();
		}// </editor-fold>
	

	public  static void main(String[] args) {
		
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new LoginFrame("陈锦霞 201621123061的登录界面").setVisible(true);
			}
		});
		
	}
	}
}

