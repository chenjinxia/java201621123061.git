package 图书管理系统;

 import java.awt.*;
 import java.awt.event.*;
 import java.sql.*;
 import javax.swing.*;
 
 public class Login extends JFrame{
 	//声明标签、按钮、文本框和密码框
 	private JLabel JLb1;
 	private JLabel JLb2; 
 	private JButton Ok_btn;
 	private JButton Cancel_btn;
 	private JTextField jtflduser;
	private JPasswordField jtpwdfld;
 	//声明窗口
 	private JFrame frame;
 	//构造方法
 	public Login(){
 		frame=new JFrame("登录");
 		Container content=frame.getContentPane();
 		//采用GridLayout布局管理器
 		content.setLayout(new GridLayout(3,2,20,20));		
 		JLb1=new JLabel("用户名");
 		JLb2=new JLabel("密   码");
 		//将标签置于居中位置
 		JLb1.setHorizontalAlignment(SwingConstants.CENTER);		
 		JLb2.setHorizontalAlignment(SwingConstants.CENTER);
 		jtflduser=new JTextField();
 		jtpwdfld=new JPasswordField();
 		Ok_btn=new JButton("确定");
 		Cancel_btn=new JButton("取消");
 		//为按钮增加事件监听者
 		Ok_btn.addActionListener(new ActionHandler());
 		Cancel_btn.addActionListener(new ActionHandler());
 		//添加标签、文本框和密码框到窗口
 		content.add(JLb1);
 		content.add(jtflduser);
 		content.add(JLb2);
 		content.add(jtpwdfld);
 		content.add(Ok_btn);
 		content.add(Cancel_btn);
 		frame.pack();
 		//设定登录窗口启动时出现在屏幕中央位置
 		frame.setLocationRelativeTo(null);
 		frame.setSize(300,200);
 		frame.setVisible(true);
 	}//Login() method

 	/**
	 *实现ActionListener监听，激活组件响应
 	 */
 	 class ActionHandler implements ActionListener{
 	 	public void actionPerformed(ActionEvent e){
 	 		String str1,str2,sqlStr;
 	 		Object obj=e.getSource();
 	 		//获得文本框和密码框的数据
 	 		str1=jtflduser.getText().trim();
 	 		str2=new String(jtpwdfld.getPassword()).trim();
 	 		try{
 	 			//点击确定按钮
 	 			if(obj.equals(Ok_btn)){
 	 				if(str1.equals("")){
 	 					JOptionPane.showMessageDialog(frame,"用户名不能为空！");
 	 					return;
 	 				}
 	 				
 	 				if(result.next()){
 	 					//弹出对话框提示登录成功
 	 					JOptionPane.showMessageDialog(frame,"登录成功！");
 	 					//打开图书管理主页面
 	 					bookmain bookmain1=new bookmain();
 	 					bookmain1.go();
 	 					//关闭登录窗口
 	 					frame.dispose();
 	 					
	 				}else{ 	 					JOptionPane.showMessageDialog(frame,"用户名或密码错误");
 	 				}
 	 				
 	 			}else if(obj.equals(Cancel_btn)){
 	 				//点击取消按钮
 	 				System.exit(0);
 	 			}
 	 		}catch(ClassNotFoundException ce){
 	 			//捕获异常
 				System.out.println("SQLException:"+ce.getMessage());	
 			}
 	 		catch(SQLException ex){
 	 			//捕获异常
 	 			System.out.println(ex);
 	 		}
 	 		catch (Exception s) {
 	 			//捕获异常
 	 			s.printStackTrace();
 	 		}
 	 	}
 	 }
 	public static void main(String args[]){
 		 	 	Login login=new Login();
 		 	 }//main			
 		 }//class ActionHandler
 