package pta;

//6-2 jmu-Java-07多线程-Runnable与停止线程（5 分）
import java.util.Scanner;
/*这里放置你的代码*/
class MonitorTask implements Runnable{
	private volatile boolean flag = false;
	private String word;
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public void stopMe(){
		flag=true;
	}
	public void sendWord(String word){
		this.word=word;
	}
	public void run(){//要判断word是否为空
		while(!flag){
			if(word!=null){
				if(word.contains("alien")){
					System.out.println(Thread.currentThread().getName()+" found alien in "+word);
					word=null;
				}
			}			
		}
		System.out.println(Thread.currentThread().getName() + " stop");
	}
}
public class Main5 {
  public static void main(String[] args) throws InterruptedException {
      MonitorTask task1 = new MonitorTask();
      Thread t1 = new Thread(task1);
      t1.start();
      Scanner sc = new Scanner(System.in);
      while (sc.hasNext()) {
          String word = sc.next();
          if (word.equals("stop")) {
              task1.stopMe();
          } else if (word.equals("quit")) {
              break;
          } else {
              if (t1.isAlive())
                 task1.sendWord(word);
          }
          Thread.sleep(10);
      }
      System.out.println(t1.isAlive());
      task1.stopMe();
      sc.close();
  }
}