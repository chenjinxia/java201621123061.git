package pta;


import java.util.Arrays;

//6-3 jmu-Java-07多线程-Runnable与匿名类（3 分）
public class Main3 {	
    public static void main(String[] args) {
        final String mainThreadName = Thread.currentThread().getName();
        Thread t1 = new Thread(new Runnable(){
        	public void run(){
        		System.out.println(mainThreadName);//主线程名
        		System.out.println(Thread.currentThread().getName());//线程t1的线程名
        		System.out.println(Arrays.toString(getClass().getInterfaces()));//线程t1所实现的所有接口
        	}
        });
        
        t1.start();
    }
}