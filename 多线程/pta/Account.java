package pta;

//6-4 jmu-Java-07多线程-互斥访问（5 分）
class Account {
	private int balance;//余额
	
	public int getBalance() {
		return balance;
	}
	public void setBalance(int balance) {
		this.balance = balance;
	}
	public Account(int balance){
		this.balance=balance;
	}
	public synchronized void deposit(int money){//存钱，在余额的基础上加上money
		balance=balance+money;
	}
	void withdraw(int money){ //取钱，在余额的基础上减去money
		balance=balance-money;
	}
}
