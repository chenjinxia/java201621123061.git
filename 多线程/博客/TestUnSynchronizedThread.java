package ����;

import java.util.concurrent.Executors;

public class TestUnSynchronizedThread {

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
	
		System.out.println("main end");
	}
}

class Adder implements Runnable {

	@Override
	public void run() {
		for (int i = 0; i < 10000; i++)
			Counter.addId();
		System.out.println(Thread.currentThread().getName() + " end");
	}
}

class Subtracter implements Runnable {

	@Override
	public void run() {
		for (int i = 0; i < 10000; i++)
			Counter.subtractId();
		System.out.println(Thread.currentThread().getName() + " end");
	}
}
//�½�ϼ201621123061
class Counter {
	private static int id = 0;

	public synchronized static void addId() {
		id++;
	}

	public synchronized static void subtractId() {
		id--;
	}

	public static int getId() {
		return id;
	}
}
