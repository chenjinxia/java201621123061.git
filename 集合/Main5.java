package 集合;

import java.util.*;
import java.util.Map.Entry;
//7-4 jmu-Java-05集合-4-倒排索引（20 分）
//Map.Entry是Map声明的一个内部接口，此接口为泛型，定义为Entry<K,V>。它表示Map中的一个实体（一个key-value对）。接口中有getKey(),getValue方法。
public class Main5{

	public static void main(String[] args) {
		
		Scanner in=new Scanner(System.in);
		String str=in.nextLine();
		Map<String,ArrayList<Integer>> map=new TreeMap<>();
		int index=1;
		ArrayList<String> sentence=new ArrayList<>();
		while(!str.equals("!!!!!")) {
			sentence.add(str);
			String []words=str.split(" ");
			for(int i=0;i<words.length;i++) {
				if(!map.containsKey(words[i])) {
					ArrayList<Integer> arraylist=new ArrayList<>();
					arraylist.add((Integer)index);
					map.put(words[i], arraylist);
				}
				else {
					ArrayList<Integer> arraylist=map.get(words[i]);
					if(!arraylist.contains(index)) {
					arraylist.add((Integer)index);
					map.put(words[i], arraylist);
					}
				}
			}
			index++;
			str=in.nextLine();
		}
		List<Entry<String,ArrayList<Integer>>> list=new ArrayList<Entry<String,ArrayList<Integer>>>(map.entrySet());
		for(int i=0;i<list.size();i++)
			System.out.println(list.get(i));
		while(true) {
			String str1=in.nextLine();
			String []str2=str1.split(" ");
			ArrayList<Integer> a=new ArrayList<>();
			ArrayList<Integer> b=new ArrayList<>();
			for(int j=0;j<str2.length;j++)
			if(map.containsKey(str2[j])) {
				if(a.isEmpty()) 
					a=map.get(str2[j]);
				else {
					b=map.get(str2[j]);
					for(int t=a.size()-1;t>=0;t--) 
						if(!b.contains(a.get(t)))
							a.remove(a.get(t));	
					if(a.isEmpty()) {
						System.out.println("found 0 results");
						break;
					}
				}
			}
			else {
				System.out.println("found 0 results");
				break;
			}
			if(!a.isEmpty())
				System.out.println(a);
			for(int i=0;i<a.size();i++) {
				System.out.println("line "+a.get(i)+":"+sentence.get(a.get(i)-1));
			}
			
		}
	}

}
		