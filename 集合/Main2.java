package ����;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

interface IntegerStack{
	public Integer push(Integer item); 
	public Integer pop();              
	public Integer peek();             
	public boolean empty();           
	public int size();                
}

class ArrayListIntegerStack implements IntegerStack{
	private List<Integer> list;
	
	public ArrayListIntegerStack(){
		list=new ArrayList<Integer>();
	}
	
	public Integer push(Integer item){
		if(item==null)
			return null;
		else
			list.add(item);
		return item;
	}

	@Override
	public Integer pop() {
		if(list.isEmpty())
			return null;
		else
			return list.remove(list.size()-1);
	}

	@Override
	public Integer peek() {
		if(list.isEmpty())
			return null;
		else
		return list.get(list.size()-1);
	}

	@Override
	public boolean empty() {
		if(list.isEmpty())
			return true;
		else
			return false;
	}

	@Override
	public int size() {
		return list.size();
	}
	
	public String toString(){
		return list.toString();
	}
}

public class Main2{
	public static void main(String[] args){
		ArrayListIntegerStack list=new ArrayListIntegerStack();
		Scanner sc = new Scanner(System.in);
		int m= sc.nextInt();
		for(int i=0;i<m;i++){
			System.out.println(list.push(sc.nextInt()));
		}
		System.out.println(list.peek()+","+list.empty()+","+list.size());
		System.out.println(list.toString());
		int x=sc.nextInt();
		for(int i=0;i<x;i++){
			System.out.println(list.pop());
		}
		System.out.println(list.peek()+","+list.empty()+","+list.size());
		System.out.println(list.toString());
		sc.close();
	}
}